/**
 * Created by paul on 31/12/14.
 */
function fizzbuzz(num){
    var result = num;

    if(num %3 == 0){
        result = 'fizz';
    }

    if(num %5 == 0){
        result = 'buzz';
    }

    if(num %3 == 0 && num %5 == 0){
        result = 'fizzbuzz';
    }

    return result;
}