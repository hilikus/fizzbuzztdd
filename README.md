# README #

a fizzbuzz example written in javascript built in a TDD style using Karma and Jasmine.

The focus here is not on the solution, but how we arrived there.

Each commit shows the initial test being created and failing (Red), with the following commit satisfying the failing test (Green).

### What is this repository for? ###

* to demonstrate TDD in Javascript
learnt from: http://karma-runner.github.io/


### How do I get set up? ###

After cloning this repository you'll need to run: npm install
note: assumes npm is already installed on your machine

This will install karma and all the dependencies I've included in the karma.conf.js (karma, karma-jasmine, karma-chrome-launcher)