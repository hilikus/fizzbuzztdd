/**
 * Created by paul on 31/12/14.
 */
describe('fizzbuzz', function() {
    it('returns the number given', function () {
        var testFunc = fizzbuzz(1);
        var testFunc2 = fizzbuzz(2);

        expect(testFunc).toEqual(1);
        expect(testFunc2).toEqual(2);
    });

    it('returns numbers divisible by 3 as fizz', function(){
        var testFunc3 = fizzbuzz(3);
        var testFunc6 = fizzbuzz(6);
        var testFunc9 = fizzbuzz(9);
        var testFunc12 = fizzbuzz(12);
        var testFunc15 = fizzbuzz(15);

        expect(testFunc3).toEqual('fizz');
        expect(testFunc6).toEqual('fizz');
        expect(testFunc9).toEqual('fizz');
        expect(testFunc12).toEqual('fizz');
    });

    it('returns numbers divisible by 5 as buzz', function(){
        var testFunc5 = fizzbuzz(5);
        var testFunc10 = fizzbuzz(10);

        expect(testFunc5).toEqual('buzz');
        expect(testFunc10).toEqual('buzz');
    });

    it('returns fizzbuzz for numbers divisible by both 3 and 5', function(){
        var testFunc15 = fizzbuzz(15);
        var testFunc30 = fizzbuzz(30);
        var testFunc45 = fizzbuzz(45);

        expect(testFunc15).toEqual('fizzbuzz');
        expect(testFunc30).toEqual('fizzbuzz');
        expect(testFunc45).toEqual('fizzbuzz');
    });
});